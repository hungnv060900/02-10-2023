package com.devcamp.countryregionjpa.repository;

import com.devcamp.countryregionjpa.model.CCountry;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
	CCountry findByCountryCodeContaining(String countryCode);
	CCountry findByCountryName(String countryName);

}
