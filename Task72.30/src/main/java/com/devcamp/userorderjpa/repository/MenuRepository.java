package com.devcamp.userorderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.CMenu;

public interface MenuRepository extends JpaRepository<CMenu, Long>{
    CMenu findById(int id);
    CMenu findByCombo(String combo);
}
