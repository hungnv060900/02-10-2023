package com.devcamp.userorderjpa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.CRegion;

public interface RegionRepository extends JpaRepository<CRegion, Long> {
    List<CRegion> findByCountryId(Long countryId);
	Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
    Optional<CRegion> findByRegionCodeAndCountryCountryCode(String regionCode, String countryCode);
	CRegion findByRegionCode(String code);
	CRegion findByRegionName(String name);
}
