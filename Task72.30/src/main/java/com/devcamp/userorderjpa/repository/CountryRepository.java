package com.devcamp.userorderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Long>{
    CCountry findByCountryCode(String countryCode);
    CCountry findByCountryCodeContaining(String countryCode);
	CCountry findByCountryName(String countryName);

}


