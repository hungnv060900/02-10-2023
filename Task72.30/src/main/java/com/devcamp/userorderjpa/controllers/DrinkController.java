package com.devcamp.userorderjpa.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.userorderjpa.model.Drink;
import com.devcamp.userorderjpa.repository.DrinkRepository;
import com.devcamp.userorderjpa.service.DrinkService;


@RestController
@RequestMapping("/")
@CrossOrigin
public class DrinkController {
    @Autowired
    DrinkService drinkService;
    
    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrinks(){
        try {
            return new ResponseEntity<>(drinkService.getAllDrinks(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("drinks/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") long id){
        try {
            Drink drink = drinkService.getDrinkById(id);
            if (drink != null){
                return new ResponseEntity<>(drink, HttpStatus.OK);
            }
            else  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @Autowired
    DrinkRepository drinkRepository;
    @PostMapping("/drinks")
	public ResponseEntity<Drink> createDrink(@RequestBody Drink pDrink) {
		try {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
			Drink _drink = drinkRepository.save(pDrink);

			return new ResponseEntity<>(_drink, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/drinks/{id}")
	public ResponseEntity<Drink> updateDrinkById(@PathVariable("id") long id, @RequestBody Drink pDrink) {
        try {
            Optional<Drink> drinkData = drinkRepository.findById(id);
            if (drinkData.isPresent()) {
                Drink drink = drinkData.get();
                drink.setMaNuocUong(pDrink.getMaNuocUong());
                drink.setTenNuocUong(pDrink.getTenNuocUong());
                drink.setPrice(pDrink.getPrice());
                drink.setNgayCapNhat(new Date());

                return new ResponseEntity<>(drinkRepository.save(drink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
	}

    @DeleteMapping("/drinks/{id}")
	public ResponseEntity<Drink> deleteDrinkById(@PathVariable("id") long id) {
		try {
			drinkRepository.deleteById(id);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

