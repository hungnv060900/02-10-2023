package com.devcamp.taskhw.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.taskhw.models.Salary;

public interface iSalaryResponsitory extends JpaRepository<Salary,Long> {
    
}
