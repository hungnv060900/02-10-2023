package com.devcamp.taskhw.services;

import org.springframework.stereotype.Service;

@Service
public class SalaryService {

    public double calculateNetSalary(double grossSalary, int dependents, double insuranceAmount, String region) {

        // Ví dụ đơn giản: Giảm 20% thuế
        double taxRate = 0.2;
        double netSalary = grossSalary - (grossSalary * taxRate);

        return netSalary;
    }
}
