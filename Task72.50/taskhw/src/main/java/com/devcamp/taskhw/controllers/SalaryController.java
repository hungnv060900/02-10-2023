package com.devcamp.taskhw.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.taskhw.models.Salary;
import com.devcamp.taskhw.responsitory.iSalaryResponsitory;
import com.devcamp.taskhw.services.SalaryService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SalaryController {

    @Autowired
    iSalaryResponsitory pSalaryResponsitory;

    @Autowired
    SalaryService salaryService;

    @PostMapping("/gross-to-net")
    public ResponseEntity<Double> calculateNetSalary(@RequestBody Salary salaryInfo) {
        double grossSalary = salaryInfo.getGrossSalary();
        int dependents = salaryInfo.getDependents();
        double insuranceAmount = salaryInfo.getInsuranceAmount();
        String region = salaryInfo.getRegion();

        // Gọi phương thức trong SalaryService để tính lương Net
        double netSalary = salaryService.calculateNetSalary(grossSalary, dependents, insuranceAmount, region);

        return ResponseEntity.ok(netSalary);
    }

}
