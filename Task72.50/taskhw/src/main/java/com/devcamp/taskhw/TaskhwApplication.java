package com.devcamp.taskhw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskhwApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskhwApplication.class, args);
	}

}
